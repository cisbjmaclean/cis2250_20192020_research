package info.hccis.canes.ui.water;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;

import info.hccis.canes.R;
import info.hccis.canes.bo.Calculator;

public class WaterFragment extends Fragment {

    private WaterViewModel galleryViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                ViewModelProviders.of(this).get(WaterViewModel.class);
        View root = inflater.inflate(R.layout.fragment_water, container, false);
        final TextView textView = root.findViewById(R.id.text_gallery);
        galleryViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        final Button calculateButton = root.findViewById(R.id.buttonCalculate);
        final EditText editTextDiameter = root.findViewById(R.id.editTextDiameter);
        final EditText editTextHeight = root.findViewById(R.id.editTextHeight);
        final EditText editTextFlowrate = root.findViewById(R.id.editTextFlowrate);

        calculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String diameter = editTextDiameter.getText().toString();
                String height = editTextHeight.getText().toString();
                String flowrate = editTextFlowrate.getText().toString();
                Log.d("bjm", "calculate button was clicked, diameter=" + diameter);

                Calculator calculator = new Calculator(diameter, height, flowrate);
                Log.d("bjm", "It will take " + calculator.getFillTimeForContainer() + " to fill the container");

                Snackbar.make(getActivity().findViewById(android.R.id.content),
                        ""+calculator.getFillTimeForContainer()+" seconds.", Snackbar.LENGTH_LONG).show();

            }
        });


        return root;
    }
}