package info.hccis.canes.util;
import info.hccis.canes.entity.Camper;
import retrofit2.Call;
import java.util.List;
import retrofit2.http.GET;

public interface JsonCamperApi {

    /**
     * This abstract method to be created to allow retrofit to get list of campers
     * @return List of campers
     * @since 20200202
     * @author BJM (with help from the retrofit research.
     */

    @GET("campers")
    Call<List<Camper>> getCampers();
}
